#!/usr/bin/env node

const _ = require('lodash');
const chalk = require('chalk');
const path = require('path');
const program = require('commander');
const slsk = require('slsk-client');
const stringSimilarity = require('string-similarity');
const Spotify = require('node-spotify-api');
const fs = require('fs');
const log = console.log;
const EventEmitter = require('events');
const VERSION = '0.0.1';
require('dotenv').config()

let queue = [];
let downloadQueue = [];
let downloadTimeStamp;
let downloadConnections = 0;
let errorCount = 0;
let isSearching = false;

class MyEmitter extends EventEmitter {}
let myEmitter = null;
let myErrorEmitter = null;
let cooldown = false;

program.version(VERSION);

program
    .command('search')
    .description('Search with required query')
    .alias('s')
    .action((options) => {
        myErrorEmitter = new MyEmitter();

        process.on('uncaughtException', function (err) {
            console.log('UNCAUGHT EXCEPTION - keeping process alive:', err); // err.message is "foobar"
            clearListeners();
        });

        myErrorEmitter.on('client_lost', () => {
            if(!cooldown) {
                cooldown = true;
                log("5 min cool down");
                setTimeout(slskProgram, 300000);
            }
        });

        slskProgram();
    });

function clearListeners() {
    if(null !== myEmitter) {
        myEmitter.removeAllListeners("spotify_refresh");
        myEmitter.removeAllListeners("search_complete");
        myEmitter.removeAllListeners("download_complete");
    }
}

function slskProgram() {
    log("Progam running!");
    cooldown = false;
    output = process.env.DOWNLOAD_FOLDER;

    myEmitter = new MyEmitter();

    slsk.connect({
        user: process.env.SOULSEEK_USER,
        pass: process.env.SOULSEEK_PASSWORD
    }, (err, client) => {

        if (err) {
            errorCount++;
            log(chalk.red(err));
            clearListeners();
            myErrorEmitter.emit('client_lost');
        }

        fetchMusic();

        myEmitter.on('spotify_refresh', () => {

            let searchQuery = getName(queue[0]);
            log(chalk.green('Searching for "%s"'), searchQuery);
            search(client, searchQuery, output, false);

        });

        myEmitter.on('search_complete', () => {
            queue.splice(0,1); //
            if(queue.length > 0) {
                isSearching = true;
                setTimeout(function () {
                    let searchQuery = getName(queue[0]);
                    log(chalk.green('Searching for "%s"'), searchQuery);
                    search(client, searchQuery, output, false)
                }, 10000);
            } else {
                isSearching = false;
            }

        });

        myEmitter.on('download_complete', () => {
            downloadConnections--;
        });

        setInterval(fetchMusic, ((1000 * 60) * 10));

    });
}

function fetchMusic() {

    const spotify = new Spotify({
        id: process.env.SPOTIFY_ID,
        secret: process.env.SPOTIFY_SECRET
    });
    if(queue.length === 0  && downloadQueue.length <= 8 && !isSearching) {
        spotify
            .request('https://api.spotify.com/v1/playlists/' + process.env.SPOTIFY_LIST + '/tracks')
            .then(function (data) {
                queue = data['items'];
                queue = queue.reverse();
                if(process.env.CUSTOM_LIST !== "") {
                    let customList = JSON.parse(fs.readFileSync(process.env.CUSTOM_LIST, 'utf8'));
                    queue = queue.concat(customList);
                }

                myEmitter.emit('spotify_refresh');
            })
            .catch(function (err) {
                console.error('Error occurred: ' + err);
            });
    }
}

function getName(spotifyItem) {
    let replaceArray = ["-", "'", "&", /\(.*?\)/g, "radio edit", "radio mix", "radio_edit", "radio remix", "version", "edit"];
    let cleanedTrackName = spotifyItem.track.name
        .toLowerCase();

    for(let i = 0; i < replaceArray.length; i++)
    {
        cleanedTrackName = cleanedTrackName.replace(replaceArray[i], "");
    }

    return spotifyItem.track.artists[0].name.toLowerCase() + " " + cleanedTrackName;
}

function search(client, item, output, similarity) {
    if(typeof client === 'undefined') {
        log("CLIENT LOST!");
        myErrorEmitter.emit('client_lost');
        return;
    }

    client.search({
        req: item,
        timeout: 2000
    }, (err, res) => {
        if (err) {
            return log(chalk.red(err));
        }

        // Keep only free slots
        res = res.filter(r => r.slots === true && r.speed > 0);
        res = res.filter(r => r.bitrate >= 320);
        res = res.filter(r => r.size / Math.pow(1024,2) <= 90);

        // Keep only mp3 / wav / flac
        res = res.filter(r => path.extname(r.file) === '.mp3' || path.extname(r.file) === '.wav' || path.extname(r.file) === '.flac')

        // Check if file already exists
        res = res.filter(r => checkIfFolderIsProcessed(output, getFilename(r.file)) === false);

        // Sort by speed
        res.sort((a, b) => b.speed - a.speed);

        // Sort by size
        res.sort((a, b) => b.size - a.size);

        let results = res;


        if(res.length > 0) {
            results.push(_.filter(res, function (o) {
                let filename = getFilename(o.file);
                return stringSimilarity.compareTwoStrings(filename, item) >= 0.80 || (filename.includes("extended") || filename.includes("original"));
            }));
        }


        let skippedFiles = 0;

        for (let i = 0; i < process.env.DOWNLOAD_SAMPLES; i++) {

            if (typeof results[i] === 'undefined' || typeof results[i].file === 'undefined' || downloadQueue.includes(results[i].file)) {
                skippedFiles++;
                log('skipped file: ' + i);
                continue;
            }

            let folderIsProcessed = checkIfFolderIsProcessed(output, getFilename(results[i].file));

            let index = downloadQueue.indexOf(results[i].file); // check whether the download is already added to the download queue.

            if(folderIsProcessed === false && index === -1) {

                if(downloadConnections < process.env.DOWNLOAD_CONNECTIONS) {
                    downloadConnections++;
                    downloadQueue.push(results[i].file);
                    console.log("download " + results[i].file);
                    download(client, results[i], output)
                } else {
                    log('skipped file: ' + i + ' because of we are out of download connections');
                    skippedFiles++;
                }

            } else {
                log('skipped file: ' + i);
                skippedFiles++;
            }

        }

        myEmitter.emit('search_complete');

    });

}

function checkIfFolderIsProcessed(outputFolder, fileName) {
    let count = 0;

    let result = false;

    if (fs.existsSync(outputFolder) === false) {
        return false;
    }

    fs.readdirSync(outputFolder).forEach(file => {
        count++;
        if(file === fileName) { // file was already downloaded/
            result = true;
        }
    });

    return result;
}

function download(client, item, output)
{
    let outputFile = output + '\\' + getFilename(item.file);

    const data = {
        file: item,
        path: outputFile
    };

    ensureDirectoryExistence(data.path);

    client.download(data, (err, down) => {
        let index = downloadQueue.indexOf(item.file);

        if (err) {
            log(chalk.red(err));
            if (index > -1) {
                log("removed " + item.file + " from download queue because of a failure");
                downloadQueue.splice(index, 1);
            }
            myEmitter.emit('download_complete');
        }

        if (index > -1) {
            log("removed " + item.file + " from download queue");
            downloadQueue.splice(index, 1);
        }

        console.log("The file was saved!");
        myEmitter.emit('download_complete');
    });
}

function ensureDirectoryExistence(filePath) {
    let dirname = path.dirname(filePath);

    if (fs.existsSync(dirname)) {
        return true;
    }

    fs.mkdirSync(dirname);
    fs.openSync(filePath, 'w');

}


function getFilename(filePath) {
    return filePath.replace(/^.*[\\\/]/, '')
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

program.parse(process.argv);
