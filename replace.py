import fileinput
import re
import os

for root, dirs, files in os.walk("."):  
    for filename in files:
        if(filename.endswith(".mp3") or filename.endswith(".flac") or filename.endswith(".wav")):
            newName = re.sub(r'(B-[0-9]*-)', r'' ,filename.rstrip())
            os.rename(filename, newName)
print "done"
            

